# Конструкция классов для создание интерактивных тестов и пример кода интеграции

Этот репозиторий представляет собой реализацию классов для тестирования с использованием JavaScript. Класс `Test` предназначен для инициализации и запуска тестов. Класс `ExternalDispatcher` используется для управления внешними событиями и передачи результатов тестов. Класс `ImagesTest` представляет собой пример теста для выбора верного изображения.


## Классы

### Test

Класс `Test` представляет основной объект тестирования. Он принимает параметры `options` (опции) и `container` (контейнер), инициализирует внешний диспетчер событий (`ExternalDispatcher`) и вызывает метод `initialize`.

#### Методы

- `initialize()`: Инициализирует тест, создавая iframe элемент в контейнере и загружая CSS файл. Если тип теста равен "imagesTest", создается экземпляр класса `ImagesTest`.

### ExternalDispatcher

Класс `ExternalDispatcher` представляет внешний диспетчер событий. Он позволяет регистрировать обратные вызовы на определенные события и вызывать их при наступлении этих событий.

#### Методы

- `on(event, callback)`: Регистрирует обратный вызов для указанного события.
- `emit(event, payload)`: Вызывает все зарегистрированные обратные вызовы для указанного события с переданным пейлоадом.
- `emitResult(result)`: Вызывает событие "result" с переданным результатом.

### ImagesTest

Класс `ImagesTest` представляет тестирование изображений. Он принимает окно (window), JSON данные и внешний диспетчер событий в качестве параметров, создает элемент div и добавляет его в тело документа внутри iframe. Кроме того, класс симулирует завершение теста через некоторое время и вызывает событие "result" через внешний диспетчер.

## Использование

Пример использования находится в конце файла и выглядит следующим образом:

```javascript
const container = document.getElementById('test-container');
const options = {
  style: 'style.css',
  json: 'data.json',
  type: 'imagesTest'
};

const test = new Test(options, container);

// Зарегистрировать обратный вызов для события "result"
test.externalDispatcher.on("result", (event) => {
  console.log("Результат события:", event.result);
});
```

